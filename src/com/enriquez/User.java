package com.enriquez;

public class User {
    // Properties
    private String firstName;
    private String lastName;
    private String contactNumber;

    // Empty Constructor
    public  User(){
        System.out.print("New User");
    }

    // Parameterized Constructor
    public User(String newfirstName, String newLastName, String newContactnumber){
        this.firstName = newfirstName;
        this.lastName = newLastName;
        this.contactNumber = newContactnumber;
    }

    // Getter
    public String getFirstName(){
        return this.firstName;
    }
    public String getLastName(){
        return this.lastName;
    }
    public String getContactNumber(){
        return this.contactNumber;
    }

    // Setter
    public void setFirstName(String newFirstName){
        this.firstName = newFirstName;
    }
    public void setLastName(String newLastName){
        this.lastName = newLastName;
    }
    public void setContactNumber(String newContactNumber){
        this.contactNumber = newContactNumber;
    }

    // Method for Displaying User Details
    public String display(){
        return "First Name: " + this.getFirstName() + ", Last Name: " + this.getLastName() + ", Contact Number " + this.getContactNumber();
    }

}
