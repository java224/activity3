package com.enriquez;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
	User a = new User ("Isaac", "Enriquez","09565790127");
    User b = new User ("Carla", "Miguel","09145790177");
    // ArrayList
    ArrayList<String> userList = new ArrayList<String>();
    userList.add(a.display());
    userList.add(b.display());
    System.out.println(userList);

    //Loop to Traverse Arraylist to invoke their details
    for (String User : userList){
        System.out.println(User);
    }
    }
}
